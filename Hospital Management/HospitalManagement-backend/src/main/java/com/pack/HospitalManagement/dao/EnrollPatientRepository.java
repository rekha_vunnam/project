package com.pack.HospitalManagement.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.HospitalManagement.model.EnrollPatient;
import com.pack.HospitalManagement.model.Physician;

public interface EnrollPatientRepository extends CrudRepository<EnrollPatient,Long> {

	

}
