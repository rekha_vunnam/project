package com.pack.HospitalManagement.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="physician")
public class Physician {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long physicianId;
	private String physicianFirstName;
	private String physicianLastName;
	private String physiciandepartment;
	private String qualification;
	private int yearsofExperience;
	private String state;
	private boolean insurancePlan;
	public Physician() {
		super();
	}
	public Physician(String physicianFirstName, String physicianLastName, String physiciandepartment,
			String qualification, int yearsofExperience, String state,Boolean insurancePlan) {
		super();
		this.physicianFirstName = physicianFirstName;
		this.physicianLastName = physicianLastName;
		this.physiciandepartment = physiciandepartment;
		this.qualification = qualification;
		this.yearsofExperience = yearsofExperience;
		this.state = state;
		this.insurancePlan=insurancePlan;
		
	}
	public Long getPhysicianId() {
		return physicianId;
	}
	public void setPhysicianId(Long physicianId) {
		this.physicianId = physicianId;
	}
	public String getPhysicianFirstName() {
		return physicianFirstName;
	}
	public void setPhysicianFirstName(String physicianFirstName) {
		this.physicianFirstName = physicianFirstName;
	}
	public String getPhysicianLastName() {
		return physicianLastName;
	}
	public void setPhysicianLastName(String physicianLastName) {
		this.physicianLastName = physicianLastName;
	}
	public String getPhysiciandepartment() {
		return physiciandepartment;
	}
	public void setPhysiciandepartment(String physiciandepartment) {
		this.physiciandepartment = physiciandepartment;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public int getYearsofExperience() {
		return yearsofExperience;
	}
	public void setYearsofExperience(int yearsofExperience) {
		this.yearsofExperience = yearsofExperience;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public boolean isInsurancePlan() {
		return insurancePlan;
	}
	public void setInsurancePlan(boolean insurancePlan) {
		this.insurancePlan = insurancePlan;
	}
	@Override
	public String toString() {
		return "Physician [physicianId=" + physicianId + ", physicianFirstName=" + physicianFirstName
				+ ", physicianLastName=" + physicianLastName + ", physiciandepartment=" + physiciandepartment
				+ ", qualification=" + qualification + ", yearsofExperience=" + yearsofExperience + ", state=" + state
				+ ", insurancePlan=" + insurancePlan + "]";
	}
	

}
