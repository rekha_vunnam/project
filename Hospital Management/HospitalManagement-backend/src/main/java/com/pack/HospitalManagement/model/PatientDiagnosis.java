package com.pack.HospitalManagement.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "diagnosis")
public class PatientDiagnosis {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long diagnosisId;
	private Long patientId;
	private String symptoms;
	private String diagnosisProvided;
	private String physicianId;
	private Date dateOfDiagnosis;
	private String followup;
	private Date followUpDate;
	private Double billAmount;
	private Long cardNumber;
	private String paymentMode;

	/**@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id")
	private EnrollPatient patient;**/

	public PatientDiagnosis() {
		super();
	}

	public PatientDiagnosis(Long patientId, String symptoms, String diagnosisProvided, String physicianId,
			Date dateOfDiagnosis, String followup, Date followUpDate, Double billAmount, Long cardNumber,
			String paymentMode) {
		super();
		this.patientId = patientId;
		this.symptoms = symptoms;
		this.diagnosisProvided = diagnosisProvided;
		this.physicianId = physicianId;
		this.dateOfDiagnosis = dateOfDiagnosis;
		this.followup = followup;
		this.followUpDate = followUpDate;
		this.billAmount = billAmount;
		this.cardNumber = cardNumber;
		this.paymentMode = paymentMode;
	}

	public Long getDiagnosisId() {
		return diagnosisId;
	}

	public void setDiagnosisId(Long diagnosisId) {
		this.diagnosisId = diagnosisId;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getDiagnosisProvided() {
		return diagnosisProvided;
	}

	public void setDiagnosisProvided(String diagnosisProvided) {
		this.diagnosisProvided = diagnosisProvided;
	}

	public String getPhysicianId() {
		return physicianId;
	}

	public void setPhysicianId(String physicianId) {
		this.physicianId = physicianId;
	}

	public Date getDateOfDiagnosis() {
		return dateOfDiagnosis;
	}

	public void setDateOfDiagnosis(Date dateOfDiagnosis) {
		this.dateOfDiagnosis = dateOfDiagnosis;
	}

	public String getFollowup() {
		return followup;
	}

	public void setFollowup(String followup) {
		this.followup = followup;
	}

	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}

	public Double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(Double billAmount) {
		this.billAmount = billAmount;
	}

	public Long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	

}