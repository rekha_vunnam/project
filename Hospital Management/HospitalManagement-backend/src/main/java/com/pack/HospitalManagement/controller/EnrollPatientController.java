package com.pack.HospitalManagement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.HospitalManagement.HospitalManagementApplication;
import com.pack.HospitalManagement.dao.EnrollPatientRepository;
import com.pack.HospitalManagement.dao.PatientDiagnosisRepository;
import com.pack.HospitalManagement.dao.PhysicianRepository;
import com.pack.HospitalManagement.model.EnrollPatient;
import com.pack.HospitalManagement.model.PatientDiagnosis;
import com.pack.HospitalManagement.model.Physician;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class EnrollPatientController {
	@Autowired
	EnrollPatientRepository repository;
	@Autowired
	PhysicianRepository physicianRepository;
	@Autowired
	PatientDiagnosisRepository diagnosisRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HospitalManagementApplication.class);

	@PostMapping(value = "/patients")
	public ResponseEntity<EnrollPatient> postEnrollPatient(@RequestBody EnrollPatient enrollPatient) {
		try {
			LOGGER.info("patient Created");
			EnrollPatient _patient = repository.save(new EnrollPatient(enrollPatient.getFirstName(),
					enrollPatient.getLastName(), enrollPatient.getPassword(), enrollPatient.getDob(),
					enrollPatient.getEmail(), enrollPatient.getContactNumber(), enrollPatient.getState(),
					enrollPatient.isInsurancePlan()));
			return new ResponseEntity<>(_patient, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}

	}
	
     
	@PostMapping(value = "/patientsDiagnosis")
	    public ResponseEntity<PatientDiagnosis> postCustomer(@RequestBody PatientDiagnosis patientDiagnosis) {
		 try {
			 LOGGER.info("Created");
	            PatientDiagnosis _patientDiagnosis = diagnosisRepository
	                    .save(new PatientDiagnosis(patientDiagnosis.getPatientId(),patientDiagnosis.getSymptoms(), patientDiagnosis.getDiagnosisProvided(),
	                            patientDiagnosis.getPhysicianId(), patientDiagnosis.getDateOfDiagnosis(),
	                            patientDiagnosis.getFollowup(), patientDiagnosis.getFollowUpDate(),
	                            patientDiagnosis.getBillAmount(), patientDiagnosis.getCardNumber(),
	                            patientDiagnosis.getPaymentMode()));
	            return new ResponseEntity<>(_patientDiagnosis, HttpStatus.CREATED);
	        } catch (Exception e) {
	            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
	        }
	    }
	            		
	@PostMapping(value = "/physicians")
	public ResponseEntity<Physician> postPhysician(@RequestBody Physician physician) {
		try {
			LOGGER.info("Created");
			
			Physician _physician = physicianRepository.save(new Physician(physician.getPhysicianFirstName(),
					physician.getPhysicianLastName(), physician.getPhysiciandepartment(), physician.getQualification(),
					physician.getYearsofExperience(), physician.getState(), physician.isInsurancePlan()));
			return new ResponseEntity<>(_physician, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}

	}

	@GetMapping("/patients")
	public ResponseEntity<List<EnrollPatient>> getAllPatients() {
		List<EnrollPatient> patients = new ArrayList<EnrollPatient>();
		try {
			LOGGER.info("List Created");
			repository.findAll().forEach(patients::add);
            System.out.println(patients);
			if (patients.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(patients, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/physicians")
	public ResponseEntity<List<Physician>> getAllPhysicians() {
		List<Physician> physicians = new ArrayList<Physician>();
		try {
			LOGGER.info("List Created");
			physicianRepository.findAll().forEach(physicians::add);

			if (physicians.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	
	@GetMapping("/patients/{id}")
	public ResponseEntity<EnrollPatient> getCustomerById(@PathVariable("id") long id) {
		Optional<EnrollPatient> patientData = repository.findById(id);

		if (patientData.isPresent()) {
			return new ResponseEntity<>(patientData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/patients/{id}")
	public ResponseEntity<HttpStatus> deleteCustomer(@PathVariable("id") long id) {
		try {
			
			repository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "physicians/physiciandepartment/{physiciandepartment}")
	public ResponseEntity<List<Physician>> findByphysiciandepartment(@PathVariable String physiciandepartment) {
		try {
			LOGGER.info("Search by department is done");
			List<Physician> physicians = physicianRepository.findByphysiciandepartment(physiciandepartment);

			if (physicians.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "physicians/state/{state}")
	public ResponseEntity<List<Physician>> findBystate(@PathVariable String state) {
		try {
			LOGGER.info("Search by state is done");
			List<Physician> physicians = physicianRepository.findBystate(state);

			if (physicians.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	/*
	 * @PutMapping(value = "/patients/update") public EnrollPatient
	 * updatePatient(@RequestBody EnrollPatient patient) {
	 * System.out.println("Into update"); System.out.println("into update" +
	 * patient.getId() + " " + patient.getName()); EnrollPatient patient1 =
	 * repository .save(new EnrollPatient(patient.getId(), patient.getFirstName(),
	 * customer.getAge(), customer.getGender())); return customer1; }
	 */
}
