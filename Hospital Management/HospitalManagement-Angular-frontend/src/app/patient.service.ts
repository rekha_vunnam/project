import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PatientService {
  private baseUrl='http://localhost:8080/api/physicians';
  private baseurl='http://localhost:8080/api/patients';
  private diagnosisurl='http://localhost:8080/api/patientsDiagnosis';
  private adminurl="http://localhost:8080/api/admin";
  constructor(private http:HttpClient) { }
  enrollPatient(patient: any):Observable<any>
  {
    return this.http.post(this.baseurl,patient);  
  }
  addPhysician(physician: any):Observable<any>
  {
    return this.http.post(this.baseUrl,physician);  
  }
  createPatientDiagnosis(diagnosis: any):Observable<any>
  {
    console.log(diagnosis);
    return this.http.post(this.diagnosisurl,diagnosis);  
  }
  
  getPatientsList():Observable<any>
  { 
    console.log("hello");
    return this.http.get(this.baseurl);
  }
  getPhysicianList():Observable<any>
  {
    return this.http.get(this.baseUrl);
  }
  getPatient(id: number): Observable<any> {
    return this.http.get(`${this.baseurl}/${id}`);   
  }
  adminLogin(admin:any):Observable<any>
  {
    return this.http.get(this.adminurl,admin);
  }
  
  deletePatient(id: number): Observable<any> {
    return this.http.delete(`${this.baseurl}/${id}`);
  }
  getByphysiciandepartment(physiciandepartment: String): Observable<any> {
    return this.http.get(`${this.baseUrl}/physiciandepartment/${physiciandepartment}`);
  }
  getByState(state: String): Observable<any> {
    return this.http.get(`${this.baseUrl}/state/${state}`);
}

//updatePatient(patient: Object): Observable<Object> {
   // return this.http.put(`${this.baseurl}` + `/update`,patient);
  //}
}
