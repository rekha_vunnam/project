import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Physician } from '../physician';
import { PatientService } from '../patient.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-physician-list',
  templateUrl: './physician-list.component.html',
  styleUrls: ['./physician-list.component.css']
})
export class PhysicianListComponent implements OnInit {
  physicians : Observable<Physician[]>
  p: number = 1;
  count: number = 5;
  constructor(private patientService:PatientService,private router:Router) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData(){
    this.physicians=this.patientService.getPhysicianList();
    console.log(this.physicians);  
}
btnClick(){
  this.router.navigateByUrl('ItemsAdmin');
}
}   
