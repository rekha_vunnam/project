import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { Observable } from 'rxjs';
import { PatientService } from '../patient.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-history',
  templateUrl: './patient-history.component.html',
  styleUrls: ['./patient-history.component.css']
})
export class PatientHistoryComponent implements OnInit {
  patients : Observable<Patient[]>
  p: number = 1;
  count: number = 5;
  constructor(private patientService:PatientService,private router:Router) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData(){
    this.patients=this.patientService.getPatientsList();
    console.log(this.patients);  
}
btnClick(){
  this.router.navigateByUrl('ItemsAdmin');
} 

}
