import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPhysicianComponent } from './add-physician/add-physician.component';
import { EnrollPatientComponent } from './enroll-patient/enroll-patient.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
//import { PatientListComponent } from './patient-list/patient-list.component';
import { PhysicianListComponent } from './physician-list/physician-list.component';
import { PatientDiagnosisComponent } from './patient-diagnosis/patient-diagnosis.component';
import { ItemsComponent } from './items/items.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuardService } from './auth-guard.service';
import { LoginComponent } from './login/login.component';
import { PatientHistoryComponent } from './patient-history/patient-history.component';


const routes: Routes = [
  {path:'ItemsAdmin/add',component:EnrollPatientComponent},
  {path:'ItemsAdmin/physicianDetails',component:AddPhysicianComponent},
  {path:'ItemsAdmin/patientsList',component:PatientHistoryComponent},
  {path:'ItemsAdmin/physicianList',component:PhysicianListComponent},
  {path:'ItemsAdmin/patientsDiagnosis',component:PatientDiagnosisComponent},
  {path:'ItemsAdmin/search',component:SearchPhysicianComponent},
  {
    path:'item',
    component:ItemsComponent
  },
  {
    path:'ItemsAdmin',
    component:AdminComponent,
    canActivate:[AuthGuardService]
},
{
  path:'Login',
  component:LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
