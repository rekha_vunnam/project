import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service';
import { Patient } from '../patient';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enroll-patient',
  templateUrl: './enroll-patient.component.html',
  styleUrls: ['./enroll-patient.component.css']
})
export class EnrollPatientComponent implements OnInit {
  patient: Patient= new Patient();
  submitted=false;
  constructor(private patientService:PatientService,private router:Router) { }

  ngOnInit(): void {
  }
 newPatient():void{
   this.submitted = false;
   this.patient=new Patient();
 }
 save()
  {
    this.patientService.enrollPatient(this.patient)
    .subscribe(
      data =>{
        console.log(data);
        this.submitted=true;
      },
      error =>console.log(error));
      this.patient=new Patient();
      }  
      
  
  onSubmit()
  {
    this.save();
  }  
  
  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
}
