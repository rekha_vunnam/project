export class Patient{
    id:number;
    firstName:String;
    lastName:String;
    password:String;
    dob:Date;
    email:String;
    contactNumber:number;
    state:String;
    insurancePlan:Boolean;
  } 