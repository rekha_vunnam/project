import { Component, OnInit, Input } from '@angular/core';
import { PatientService } from '../patient.service';
import { PatientListComponent } from '../patient-list/patient-list.component';
import { Router } from '@angular/router';
import { Patient } from '../patient';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {
 @Input() patient: Patient;
  constructor(private patientService:PatientService,private listComponent:PatientListComponent,private router:Router) { }

  ngOnInit(): void {
  }
  editPatient(patient:Patient):void{
    console.log("into edit");
    localStorage.setItem("id",patient.id.toString());
    this.router.navigate(["edit"]);
  }
  deletePatient() {
    this.patientService.deletePatient(this.patient.id)
      .subscribe(
        data => {
          console.log(data);
          this.listComponent.reloadData();
        },
        error => console.log(error));
  }  
  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
} 
