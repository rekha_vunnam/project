import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PatientDiagnosis } from '../diagnosis';
import { Patient } from '../patient';
import { PatientService } from '../patient.service';
import { Physician } from '../physician';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-diagnosis',
  templateUrl: './patient-diagnosis.component.html',
  styleUrls: ['./patient-diagnosis.component.css']
})
export class PatientDiagnosisComponent implements OnInit {

  patientDiagnosis: PatientDiagnosis = new PatientDiagnosis();
  submitted = false;
 
  patients: Observable<Patient[]>;
  physicians:Observable<Physician[]>
  constructor(private patientService: PatientService,private router:Router) { }
 
  ngOnInit(): void {
    this.reloadData();
  }
  reloadData(){
    this.patients = this.patientService.getPatientsList();
    this.physicians = this.patientService.getPhysicianList();
    console.log(this.patients);
  }
 
  newPatientDiagnosis(): void{
    this.submitted = false;
    this.patientDiagnosis = new PatientDiagnosis();
  }
 
  onSubmit(){
    this.save();
  }
 
  save(){
    console.log(this.patientDiagnosis);
    this.patientService.createPatientDiagnosis(this.patientDiagnosis)
  .subscribe(
    data=> {
      console.log(data);
      this.submitted = true;
    },
    error => console.log(error));
    this.patientDiagnosis = new PatientDiagnosis();
    
  }
  
  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
}
