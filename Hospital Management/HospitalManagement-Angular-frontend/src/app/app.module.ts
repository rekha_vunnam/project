import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AddPhysicianComponent } from './add-physician/add-physician.component';
import { EnrollPatientComponent } from './enroll-patient/enroll-patient.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { PhysicianListComponent } from './physician-list/physician-list.component';
import { PatientDiagnosisComponent } from './patient-diagnosis/patient-diagnosis.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { ItemsComponent } from './items/items.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PatientHistoryComponent } from './patient-history/patient-history.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPhysicianComponent,
    EnrollPatientComponent,
    SearchPhysicianComponent,
    PatientListComponent,
    PatientDetailsComponent,
    PhysicianListComponent,
    PatientDiagnosisComponent,
    AdminComponent,
    LoginComponent,
    ItemsComponent,
    PatientHistoryComponent, 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    NgxPaginationModule,
    FormsModule,  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
    