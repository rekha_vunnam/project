import { Component, OnInit } from '@angular/core';
import { Physician } from '../physician';
import { PatientService } from '../patient.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-physician',
  templateUrl: './search-physician.component.html',
  styleUrls: ['./search-physician.component.css']
})
export class SearchPhysicianComponent implements OnInit {
  physiciandepartment: String;
  state: String;
  physicians: Physician[];
  constructor(private patientService:PatientService,private router:Router) { }

  ngOnInit(): void {
    this.physiciandepartment = null;
    this.state=null;
    
  }
   private searchPhysicians() {
    this.physicians = [];
    this.patientService.getByphysiciandepartment(this.physiciandepartment)
      .subscribe(physicians => this.physicians = physicians);
  }
  private searchByState(){
    this.physicians = [];
    this.patientService.getByState(this.state)
      .subscribe(physicians => this.physicians = physicians);
  }
  onSubmit() {
    this.searchPhysicians();
    this.searchByState();
     
  }
  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
  
  }

  
