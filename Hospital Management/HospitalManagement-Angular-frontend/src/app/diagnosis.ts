export class PatientDiagnosis{
    patientId:number;
    diagnosisId:number;
    symptoms:String;
    diagnosisProvided:String;
    physicianId:String;
   dateOfDiagnosis:Date;
    followup:string;
    followUpDate:Date;
    billAmount:number;
    cardNumber:number;
    paymentMode:String;
  } 