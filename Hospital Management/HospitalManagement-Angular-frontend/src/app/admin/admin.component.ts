import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service';
import { Router } from '@angular/router';
import { AuthServiceService } from '../auth-service.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  
  constructor(private route:Router,private _authService: AuthServiceService) { }
   ngOnInit(): void {
  }
  logout():void
    {​​​​​​​​
  console.log("logout");
  this._authService.logOut();
  this.route.navigate(['/item']);
    }​​​​​​​​
   
}

