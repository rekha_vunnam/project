import { Component, OnInit } from '@angular/core';
import { Physician } from '../physician';
import { PatientService } from '../patient.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-add-physician',
  templateUrl: './add-physician.component.html',
  styleUrls: ['./add-physician.component.css']
})
export class AddPhysicianComponent implements OnInit {

  physician:Physician= new Physician();
  submitted=false;
  constructor(private patientService:PatientService,private router:Router) { }

  ngOnInit(): void {
  }
  newPhysician():void{
   this.submitted = false;
   this.physician=new Physician();
 } 
 save()
  {
    this.patientService.addPhysician(this.physician)
    .subscribe(
      data =>{
        console.log(data);
        this.submitted=true;
      },
      error =>console.log(error));
      this.physician=new Physician();
      }
    
  
  onSubmit()
  {
    this.save();
  }   
  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
} 

