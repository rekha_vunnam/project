import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service';
import { Observable } from 'rxjs';
import { Patient } from '../patient';


@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
 patients:Observable<Patient[]>;
  constructor(private patientService:PatientService) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData()
  {
    this.patients=this.patientService.getPatientsList();
  }
   

}
